# [checkout-component-react]

Librería React para agregar los componentes del formulario CheckoutJS, desarrollado por el proyecto [API-PAGOS]

## Pre-Instalación
Debe haber configurado el archivo .npmrc

## Instalación
```
npm install checkout-component-react
```
## Uso
#### Javascript
``` js
import { TagCheckout } from 'checkout-component-react';
```
## Documentación y ejemplos

#### ¿Cómo muestro las entradas del formulario?
Funciona como cualquier componente creado alguna vez en react.

```html
    <TagCheckout type="email" />
    <TagCheckout type="name" />
    <TagCheckout type="lastName"/>
    <TagCheckout type="phone"/>
    <TagCheckout type="card"/>
    <TagCheckout type="cvv"/>
    <TagCheckout type="expiry"/>
    <TagCheckout type="typeDoc"/>
    <TagCheckout type="nroDoc"/>
    <TagCheckout type="installments" items="24" />
    <TagCheckout type="subscription"/>
    <p id="validMessage"></p>
    <TagCheckout type="button" />
```

#### ¿Qué atributos recibe cada campo y cómo funciona?
Todos los detalles están en la [documentación] de integración.