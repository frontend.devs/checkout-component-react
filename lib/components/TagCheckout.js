"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _Tag = _interopRequireDefault(require("./Tag"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var TagCheckout =
/*#__PURE__*/
function (_Component) {
  _inherits(TagCheckout, _Component);

  function TagCheckout(props) {
    var _this;

    _classCallCheck(this, TagCheckout);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(TagCheckout).call(this, props));

    _defineProperty(_assertThisInitialized(_this), "componentDidMount", function () {
      var _this$props = _this.props,
          type = _this$props.type,
          title = _this$props.title,
          items = _this$props.items;
      var tagsAllow = ['email', 'name', 'lastName', 'phone', 'card', 'cvv', 'expiry', 'typeDoc', 'nroDoc', 'installments', 'subscription', 'button'];

      if (tagsAllow.find(function (tg) {
        return tg === type;
      })) {
        _this.setState({
          idTag: "checkout[".concat(type, "]"),
          valueTag: window.settings[type]
        });

        if (type === 'typeDoc' || type === 'installments') {
          var optionsArr = [];

          if (type === 'typeDoc') {
            optionsArr = [{
              text: '- Seleccione -',
              value: '0'
            }, {
              text: 'DNI',
              value: '1'
            }, {
              text: 'C. EXTRANJERIA',
              value: '4'
            }, {
              text: 'PASAPORTE',
              value: '7'
            }];
          } else {
            var installments = items || '12';
            installments = Number.isNaN(installments) ? 12 : parseInt(installments);
            installments = installments > 36 ? 36 : installments;
            optionsArr = [];

            for (var index = 1; index <= installments; index += 1) {
              optionsArr.push({
                text: "".concat(index === 1 ? 'Sin' : index, " cuotas"),
                value: index
              });
            }
          }

          _this.setState({
            tag: 'select',
            optionsArr: optionsArr
          });
        } else if (type === 'button') {
          if (window.settings.operation === 'payment') {
            var currencyLocal = window.settings.currency === 'PEN' ? 'S/ ' : '$ ';

            _this.setState({
              idTag: 'btnPayment',
              tag: 'button',
              valueTag: title || "PAGAR ".concat(currencyLocal, " ").concat(window.settings.amount)
            });
          } else {
            _this.setState({
              idTag: 'btnSubscribe',
              tag: 'button',
              valueTag: title || 'SUSCRIBIR'
            });
          }
        } else {
          var maxLength = '';

          switch (type) {
            case 'phone':
              maxLength = 15;
              break;

            case 'nroDoc':
              maxLength = 8;
              break;

            case 'card':
              maxLength = 20;
              break;

            case 'cvv':
              maxLength = 4;
              break;

            case 'expiry':
              maxLength = 5;
              break;

            default:
              maxLength = '';
              break;
          }

          _this.setState({
            maxLengthTag: maxLength
          });
        }
      } else {
        throw new TypeError('Tipo de tag no soportado.');
      }
    });

    _defineProperty(_assertThisInitialized(_this), "changeData", function (e) {
      _this.setState({
        valueTag: e.target.value
      });
    });

    _this.state = {
      idTag: '',
      valueTag: '',
      maxLengthTag: '',
      tag: '',
      optionsArr: []
    };
    return _this;
  }

  _createClass(TagCheckout, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$state = this.state,
          valueTag = _this$state.valueTag,
          tag = _this$state.tag,
          optionsArr = _this$state.optionsArr,
          idTag = _this$state.idTag,
          maxLengthTag = _this$state.maxLengthTag;
      return _react["default"].createElement(_Tag["default"], {
        valueTag: valueTag,
        tag: tag,
        optionsArr: optionsArr,
        changeData: function changeData(e) {
          return _this2.changeData(e);
        },
        idTag: idTag,
        maxLengthTag: maxLengthTag
      });
    }
  }]);

  return TagCheckout;
}(_react.Component);

exports["default"] = TagCheckout;