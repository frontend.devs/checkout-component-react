"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var Text = function Text(_ref) {
  var idTag = _ref.idTag,
      changeData = _ref.changeData,
      maxLengthTag = _ref.maxLengthTag,
      valueTag = _ref.valueTag,
      _ref$classTag = _ref.classTag,
      classTag = _ref$classTag === void 0 ? "" : _ref$classTag,
      _ref$attributes = _ref.attributes,
      attributes = _ref$attributes === void 0 ? {} : _ref$attributes;
  return _react["default"].createElement("input", _extends({
    type: "text",
    onChange: function onChange(e) {
      return changeData(e);
    },
    maxLength: maxLengthTag,
    id: idTag,
    value: valueTag,
    className: classTag
  }, attributes));
};

var _default = Text;
exports["default"] = _default;