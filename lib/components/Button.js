"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var Button = function Button(_ref) {
  var idTag = _ref.idTag,
      valueTag = _ref.valueTag,
      _ref$classTag = _ref.classTag,
      classTag = _ref$classTag === void 0 ? "" : _ref$classTag;
  return _react["default"].createElement("button", {
    type: "button",
    id: idTag,
    disabled: true,
    className: classTag
  }, valueTag) // <input type="button" id={idTag} disabled value={valueTag} className={classTag} />
  ;
};

var _default = Button;
exports["default"] = _default;