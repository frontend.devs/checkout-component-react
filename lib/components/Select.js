"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var Select = function Select(_ref) {
  var valueTag = _ref.valueTag,
      idTag = _ref.idTag,
      _ref$options = _ref.options,
      options = _ref$options === void 0 ? [] : _ref$options,
      _ref$classTag = _ref.classTag,
      classTag = _ref$classTag === void 0 ? "" : _ref$classTag,
      _ref$attributes = _ref.attributes,
      attributes = _ref$attributes === void 0 ? {} : _ref$attributes;
  return _react["default"].createElement("select", _extends({
    defaultValue: valueTag,
    id: idTag,
    className: classTag
  }, attributes), options.map(function (op, id) {
    return _react["default"].createElement("option", {
      key: id,
      value: op.value
    }, op.text);
  }));
};

var _default = Select;
exports["default"] = _default;