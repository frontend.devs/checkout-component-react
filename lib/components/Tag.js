"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _Select = _interopRequireDefault(require("./Select"));

var _Text = _interopRequireDefault(require("./Text"));

var _Button = _interopRequireDefault(require("./Button"));

var _Checkbox = _interopRequireDefault(require("./Checkbox"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var Tag = function Tag(_ref) {
  var _ref$valueTag = _ref.valueTag,
      valueTag = _ref$valueTag === void 0 ? "" : _ref$valueTag,
      _changeData = _ref.changeData,
      _ref$idTag = _ref.idTag,
      idTag = _ref$idTag === void 0 ? "" : _ref$idTag,
      _ref$maxLengthTag = _ref.maxLengthTag,
      maxLengthTag = _ref$maxLengthTag === void 0 ? "" : _ref$maxLengthTag,
      _ref$tag = _ref.tag,
      tag = _ref$tag === void 0 ? "" : _ref$tag,
      optionsArr = _ref.optionsArr,
      classTag = _ref.classTag,
      _ref$attributes = _ref.attributes,
      attributes = _ref$attributes === void 0 ? {} : _ref$attributes;
  return tag === 'select' ? _react["default"].createElement(_Select["default"], {
    valueTag: valueTag,
    idTag: idTag,
    options: optionsArr,
    classTag: classTag,
    attributes: attributes
  }) : tag === 'button' ? _react["default"].createElement(_Button["default"], {
    idTag: idTag,
    valueTag: valueTag,
    classTag: classTag
  }) : idTag !== 'checkout[subscription]' ? _react["default"].createElement(_Text["default"], {
    valueTag: valueTag,
    changeData: function changeData(e) {
      return _changeData(e);
    },
    maxLengthTag: maxLengthTag,
    idTag: idTag,
    classTag: classTag,
    attributes: attributes
  }) : _react["default"].createElement(_Checkbox["default"], {
    idTag: idTag,
    classTag: classTag,
    attributes: attributes
  });
};

var _default = Tag;
exports["default"] = _default;